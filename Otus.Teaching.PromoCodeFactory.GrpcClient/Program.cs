﻿using System;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Net.Client;
using Otus.Teaching.PromoCodeFactory.WebHost;


namespace Otus.Teaching.PromoCodeFactory.GrpcClient
{
    internal class Program
    {
        private static async Task Main()
        {
            // The port number(5001) must match the port of the gRPC server.
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Customers.CustomersClient(channel);
            
            var customers =  client.GetCustomers(
                new CustomersRequest());

            foreach (var customer in customers.Items)
            {
                Console.WriteLine($"{customer.Id},{customer.Email},{customer.FirstName},{customer.LastName}");
            }

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
