using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Dto;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Areas.Admin.Pages.Customers
{
    public class EditModel : PageModel
    {

        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public EditModel(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public string PageTitle { get; set; }

        [ModelBinder]
        public CustomerCreateOrEditDto CustomerModel { get; set; }
        public async Task<IActionResult> OnGetAsync(Guid id)
        {
            PageTitle = "�������������� �������";

            var customerFromDB = await _customerRepository.GetByIdAsync(id);

            CustomerModel = new CustomerCreateOrEditDto
            {
                Id = customerFromDB.Id,
                Email = customerFromDB.LastName,
                FirstName = customerFromDB.FirstName,
                LastName = customerFromDB.LastName
            };

            var preferences = await _preferenceRepository.GetAllAsync();

            CustomerModel.Preferences = preferences.Select(c => new SelectListItem() { Text = c.Name, Value = c.Id.ToString(), Selected = customerFromDB.Preferences.Any(x => x.PreferenceId == c.Id) }).ToList();

            return Page();
        }
        public async Task<IActionResult> OnPostAsync(Guid id)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Customer customerFromBd = await _customerRepository.GetByIdAsync(CustomerModel.Id);

            customerFromBd.Email = CustomerModel.Email;
            customerFromBd.FirstName = customerFromBd.FirstName;
            customerFromBd.LastName = customerFromBd.LastName;
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(CustomerModel.PreferencesIds);

            customerFromBd.Preferences = preferences.Select(x => new CustomerPreference()
            {
                PreferenceId = x.Id,
                CustomerId = CustomerModel.Id,
                Preference = x
            }).ToList();

            try
            {
                await _customerRepository.UpdateAsync(customerFromBd);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return RedirectToPage("./Index");
        }
    }
}
