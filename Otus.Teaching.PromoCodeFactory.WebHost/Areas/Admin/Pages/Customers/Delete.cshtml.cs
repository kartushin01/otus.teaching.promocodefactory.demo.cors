using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Areas.Admin.Pages.Customers
{
    public class DeleteModel : PageModel
    {
        private readonly IRepository<Customer> _repository;
        public async Task<IActionResult> OnGet(Guid id)
        {
            var toDelete = await _repository.GetByIdAsync(id);

            if (toDelete != null)
            {
                await _repository.DeleteAsync(toDelete);
            }

            return RedirectToPage("/Customers/Index");
        }
    }
}
