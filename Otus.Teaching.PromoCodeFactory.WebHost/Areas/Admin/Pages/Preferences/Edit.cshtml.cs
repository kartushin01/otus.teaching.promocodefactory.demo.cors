using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Areas.Admin.Pages.Preferences
{
    public class EditModel : PageModel
    {
        private readonly IRepository<Preference> _repository;

        public EditModel(IRepository<Preference> repository)
        {
            _repository = repository;
        }

        public string PageTitle { get; set; }


        [ModelBinder]
        public PreferenceResponse Model { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid id)
        {
            PageTitle = "�������� ������������";

            var preferenceFromDb = await _repository.GetByIdAsync(id);

            Model = PreferenceMapper.MappingFromDb(preferenceFromDb);

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Core.Domain.PromoCodeManagement.Preference preference = await _repository.GetByIdAsync(Model.Id);

            var prefereceToEdit = PreferenceMapper.MappinfFromModel(Model, preference);

            try
            {
                await _repository.UpdateAsync(prefereceToEdit);
            }
            catch (Exception e)
            {
               throw new Exception(e.Message);
            }

            return RedirectToPage("./Index");
        }
    }
}
