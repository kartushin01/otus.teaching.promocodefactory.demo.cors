using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Areas.Admin.Pages.Preferences
{
    public class IndexModel : PageModel
    {
        private readonly IRepository<Core.Domain.PromoCodeManagement.Preference>
            _preferenceRepository;


        public List<PreferenceResponse> Preferences { get; set; }
        public string PageTitle { get; set; }

        public IndexModel(IRepository<Core.Domain.PromoCodeManagement.Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        public async Task OnGet()
        {
            PageTitle = "������ ������������";

            var preferences = await _preferenceRepository.GetAllAsync();
            Preferences = preferences.Select(x=> new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
        }
    }
}
