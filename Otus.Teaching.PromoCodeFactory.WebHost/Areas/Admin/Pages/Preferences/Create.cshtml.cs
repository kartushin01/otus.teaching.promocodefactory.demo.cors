using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Areas.Admin.Pages.Preferences
{
    public class CreateModel : PageModel
    {
        private readonly IRepository<Preference> _repository;

        public CreateModel(IRepository<Preference> repository)
        {
            _repository = repository;
        }

        public string PageTitle { get; set; }

        [ModelBinder]
        public PreferenceResponse Model { get; set; }


        public async Task<IActionResult> OnGet()
        {
            PageTitle = "������� ������������";

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {

            if (!ModelState.IsValid)
            {
                return Page();
            }

            var preference = PreferenceMapper.MappinfFromModel(Model);

            try
            {
                await _repository.AddAsync(preference);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }


            return RedirectToPage("./Index");


        }
    }
}
