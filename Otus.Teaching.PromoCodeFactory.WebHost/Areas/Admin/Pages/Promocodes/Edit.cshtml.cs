using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Dto;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Areas.Admin.Pages.Promocodes
{
    public class EditModel : PageModel
    {
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Partner> _partneRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Employee> _employeRepository;
        private readonly IRepository<PromoCode> _promoRepository;

        public EditModel(IRepository<Preference> preferenceRepository, IRepository<Partner> partneRepository, IRepository<Customer> customerRepository, IRepository<Employee> employeRepository, IRepository<PromoCode> promoRepository)
        {
            _preferenceRepository = preferenceRepository;
            _partneRepository = partneRepository;
            _customerRepository = customerRepository;
            _employeRepository = employeRepository;
            _promoRepository = promoRepository;
        }
        public string PageTitle { get; set; }

        [ModelBinder]
        public PromoCodeCreateOrEditDto PromocodeModel { get; set; }
        public async Task<IActionResult> OnGetAsync(Guid id)
        {
            PageTitle = "�������������� ���������";

            var codedFromDb = await _promoRepository.GetByIdAsync(id);

            if (codedFromDb == null)
            {
                return NotFound();
            }

            var preferences = await _preferenceRepository.GetAllAsync();
            var mangares = await _employeRepository.GetAllAsync();

            PromocodeModel = PromoCodeMapper.MapToDto(codedFromDb, preferences, mangares);

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var codeFromDb = await _promoRepository.GetByIdAsync(PromocodeModel.Id);
            var preference = await 
                _preferenceRepository.GetByIdAsync(PromocodeModel.PreferenceId);

            var employe =
                await _employeRepository.GetByIdAsync(PromocodeModel.PartnerId);

            IEnumerable<Customer> customers = await _customerRepository.GetWhere(x => x.Preferences.Any(x => x.PreferenceId == preference.Id));

            var codeToEdit = PromoCodeMapper.MapFromDto(PromocodeModel, preference, employe, customers, codeFromDb);

            try
            {
                await _promoRepository.UpdateAsync(codeToEdit);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return RedirectToPage("./Index");
        }
    }
}
