using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Dto;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Areas.Admin.Pages.Promocodes
{
    public class CreateModel : PageModel
    {

        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Partner> _partneRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Employee> _employeRepository;
        private readonly IRepository<PromoCode> _promoRepository;

        public CreateModel(IRepository<Preference> preferenceRepository, IRepository<Partner> partneRepository, IRepository<Customer> customerRepository, IRepository<Employee> employeRepository, IRepository<PromoCode> promoRepository)
        {
            _preferenceRepository = preferenceRepository;
            _partneRepository = partneRepository;
            _customerRepository = customerRepository;
            _employeRepository = employeRepository;
            _promoRepository = promoRepository;
        }

        public string PageTitle { get; set; }

        [ModelBinder]
        public PromoCodeCreateOrEditDto PromocodeModel { get; set; }
        public async Task OnGet()
        {
            PageTitle = "Создание промокода";

            PromocodeModel = new PromoCodeCreateOrEditDto();

            var preferences = await _preferenceRepository.GetAllAsync();

            PromocodeModel.Preferences = preferences.Select(c => new SelectListItem() { Text = c.Name, Value = c.Id.ToString() }).ToList();

            var mangares = await _employeRepository.GetAllAsync();

            PromocodeModel.Partners = mangares.Select(c => new SelectListItem() { Text = c.FullName, Value = c.Id.ToString() }).ToList();

        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Preference preference = await _preferenceRepository.GetByIdAsync(PromocodeModel.PreferenceId);

            IEnumerable<Customer> customers = await _customerRepository.GetWhere(x => x.Preferences.Any(x => x.PreferenceId == preference.Id));

            Employee partnter = await _employeRepository.GetByIdAsync(PromocodeModel.PartnerId);

            var createdPromocode = PromoCodeMapper.MapFromDto(PromocodeModel, preference, partnter, customers);

            await _promoRepository.AddAsync(createdPromocode);

            return RedirectToPage("./Index");
        }
    }
}
