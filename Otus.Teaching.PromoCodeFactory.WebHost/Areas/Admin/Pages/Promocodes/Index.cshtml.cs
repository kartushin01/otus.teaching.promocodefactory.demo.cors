using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Areas.Admin.Pages.Promocodes
{
    public class IndexModel : PageModel
    {
        private readonly IRepository<PromoCode> _promoRepository;
        public List<PromoCodeShortResponse> PromoCodesModel { get; set; }
        
        public IndexModel(IRepository<PromoCode> promoRepository)
        {
            _promoRepository = promoRepository;
        }

        public string PageTitle { get; set; }
        public async Task OnGetAsync()
        {
            PageTitle = "Список промокодов";

            var promocodes = await _promoRepository.GetAllAsync();

            PromoCodesModel = promocodes.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                PartnerName = x.PartnerName,
                Code = x.Code,
                BeginDate = x.BeginDate.ToString("g"),
                EndDate = x.EndDate.ToString("g"),
                ServiceInfo = x.ServiceInfo
            }).ToList();
        }
    }
}
