﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotChocolate.Types;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL.Types
{
    public class PreferenceType : ObjectType<Preference>
    {
        protected override void Configure(IObjectTypeDescriptor<Preference> descriptor)
        {
            descriptor.Field(a => a.Id).Type<StringType>();
            descriptor.Field(a => a.Name).Type<StringType>();
        }
    }
}

