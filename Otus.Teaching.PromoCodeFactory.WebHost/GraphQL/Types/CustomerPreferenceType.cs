﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotChocolate.Types;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL.Types
{
    public class CustomerPreferenceType : ObjectType<CustomerPreference>
    {
        protected override void Configure(IObjectTypeDescriptor<CustomerPreference> descriptor)
        {
            descriptor.Field(a => a.PreferenceId).Type<StringType>();
            descriptor.Field(a => a.CustomerId).Type<StringType>();
        }
    }
}
