﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class AddPreferenceInput
    {
        public AddPreferenceInput(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}
