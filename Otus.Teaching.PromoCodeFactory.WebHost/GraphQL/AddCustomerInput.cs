﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class AddCustomerInput
    {
        public AddCustomerInput(string firstName, string lastName, string email, string[] preferencesIds)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            PreferencesIds = preferencesIds;
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string[] PreferencesIds { get; set; }
        

    }
}
