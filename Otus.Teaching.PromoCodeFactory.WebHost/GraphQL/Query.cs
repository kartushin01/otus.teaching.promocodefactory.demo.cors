﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotChocolate;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class Query
    {
        public IQueryable<Customer> GetCustomers([Service] DataContext context) => context.Customers;
        public IQueryable<Preference> GetPreferences([Service] DataContext context) => context.Preferences;

    }
}
