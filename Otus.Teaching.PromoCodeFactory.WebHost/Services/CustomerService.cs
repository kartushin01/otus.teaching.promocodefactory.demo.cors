﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.Collections;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;


namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class CustomerService : Customers.CustomersBase
    {
        private readonly ILogger<CustomerService> _logger;
        private readonly IRepository<Customer> _repository;
        public CustomerService(ILogger<CustomerService> logger, IRepository<Customer> repository)
        {
            _logger = logger;
            _repository = repository;
        }

        
        public override async Task<CustomersResponse> GetCustomers(
            CustomersRequest request,
            ServerCallContext context)
        {

            var customers = await _repository.GetAllAsync();

            var response = new CustomersResponse();

            foreach (var customer in customers)
            {
                response.Items.Add(new CustomersDTO()
                {
                    Id = customer.Id.ToString(),
                    Email = customer.Email,
                    LastName = customer.LastName,
                    FirstName = customer.FirstName
                });
            };

            return await Task.FromResult(response);
        }
    }

}
