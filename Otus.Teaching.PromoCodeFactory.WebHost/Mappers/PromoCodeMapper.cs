﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.WebHost.Dto;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapFromModel(GivePromoCodeRequest request,
            Preference preference,
            IEnumerable<Customer> customers)
        {

            var promocode = new PromoCode
            {
                Id = Guid.NewGuid(),

                PartnerName = request.PartnerName,
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,

                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(30),

                Preference = preference,
                PreferenceId = preference.Id,

                Customers = new List<PromoCodeCustomer>()
            };

            foreach (var item in customers)
            {
                promocode.Customers.Add(new PromoCodeCustomer()
                {

                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = promocode.Id,
                    PromoCode = promocode
                });
            }

            return promocode;
        }

        public static PromoCode MapFromDto(PromoCodeCreateOrEditDto model, Preference preference, Employee partner, IEnumerable<Customer> customers, PromoCode promocode = null)
        {
            if (promocode == null)
            {
                promocode = new PromoCode();
                promocode.Id = Guid.NewGuid();
            }

            promocode.PartnerName = partner.FullName;
            promocode.PartnerManager = partner;
            promocode.Code = model.Code;
            promocode.ServiceInfo = model.ServiceInfo;
            promocode.BeginDate = model.BeginDate;
            promocode.EndDate = model.EndDate;
            promocode.Preference = preference;
            promocode.PreferenceId = preference.Id;
            
            promocode.Customers = customers.Select(x => new PromoCodeCustomer()
            {
                CustomerId = x.Id,
                Customer = x,
                PromoCodeId = promocode.Id,
                PromoCode = promocode
            }).ToList();


            return promocode;
        }

        public static PromoCodeCreateOrEditDto MapToDto(PromoCode model,
            IEnumerable<Preference> preferences,
            IEnumerable<Employee> employees)
        {
            var dto = new PromoCodeCreateOrEditDto() { };

            dto.Id = model.Id;
            dto.Code = model.Code;
            dto.ServiceInfo = model.ServiceInfo;
            dto.PreferenceId = model.PreferenceId;
            dto.PartnerId = (Guid)model.PartnerManagerId;
            dto.Preferences = preferences.Select(c => new SelectListItem() { Text = c.Name, Value = c.Id.ToString(), Selected = model.Preference.Id == c.Id }).ToList();
            dto.Partners = employees.Select(c => new SelectListItem() { Text = c.FullName, Value = c.Id.ToString(), Selected = model.PartnerManagerId == c.Id }).ToList();

            return dto;
        }
    }
}
